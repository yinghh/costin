package com.yhh.data.controller;

import com.yhh.data.entity.Teacher;
import com.yhh.data.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/teacher")
public class TeacherController extends AbstractController {

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    @ResponseBody
    public void toLogin(HttpServletResponse response, HttpServletRequest request) throws IOException {
        Teacher teacher = teacherService.selectTeacher(2);
        doPostData(response, teacher);
    }

}
