package com.yhh.data.dao;

import com.yhh.data.entity.Book;

import java.util.Collection;
import java.util.List;
import java.util.Map;


public interface BookDao {


    /**
     * 根据ID查询数据
     */
    Book selectBook(Integer id);

    /**
     * 根据ID查询数据
     */
    Map<Integer, Book> selectBookByIds(Collection<Integer> ids);


    /**
     * hql 查询
     */
    List<Book> selectAllByQuery();


    /**
     * sql 查询
     */
    List<Object[]> selectAllBySqlQuery();

    /**
     * 保存数据
     */
    Integer saveBook(Book book);

    /**
     * 更新数据
     */
    void updateBook(Book book);

    /**
     * 删除数据
     */
    void deleteBook(Book book);
}
