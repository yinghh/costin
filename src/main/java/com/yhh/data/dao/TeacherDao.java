package com.yhh.data.dao;


import com.yhh.data.entity.Teacher;


public interface TeacherDao {


    /**
     * 根据ID查询数据
     */
    Teacher selectTeacher(Integer id);

    /**
     * 保存数据
     */
    Integer saveTeacher(Teacher teacher);

    /**
     * 更新数据
     */
    void updateTeacher(Teacher teacher);

    /**
     * 删除数据
     */
    void deleteTeacher(Teacher teacher);


}
