package com.yhh.data.daoimpl;


import com.yhh.data.dao.BookDao;
import com.yhh.data.entity.Book;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class BookDaoImpl extends AbstractDaoImpl<Book> implements BookDao {

    @Override
    public Book selectBook(Integer id) {
        return select(id);
    }

    @Override
    public Map<Integer, Book> selectBookByIds(Collection<Integer> ids) {
        List<Book> books = loads(ids);
        Map<Integer, Book> result = new LinkedHashMap<>();
        for (Book book : books) {
            result.put(book.getId(), book);
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> selectAllByQuery() {
        Session session = getSession();
        String hql = "from Book";
        Query query = session.createQuery(hql);
        return query.list();
    }

    @Override
    public List<Object[]> selectAllBySqlQuery() {
        Session session = getSession();
        String sql = "select id,name,create_time,update_time from Book";
        Query query = session.createSQLQuery(sql);
        return query.list();
    }

    @Override
    public Integer saveBook(Book book) {
        return (Integer) save(book);
    }

    @Override
    public void updateBook(Book book) {
        update(book);
    }

    @Override
    public void deleteBook(Book book) {
        delete(book);
    }
}
