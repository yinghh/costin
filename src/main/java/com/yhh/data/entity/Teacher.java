package com.yhh.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Setter
@Getter
public class Teacher implements Serializable {
    private static final long serialVersionUID = 6318448328892767701L;

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", title='" + title + '\'' +
                '}';
    }

    @Id
    private int id;
    private String name;
    private String title;
}