package com.yhh.data.utils;

import java.util.Objects;

public class ClassUtils {
    public static Class<?> filterCglibProxyClass(Class<?> theClass) {
        Objects.requireNonNull(theClass);
        while (isCglibProxyClass(theClass)) {
            theClass = theClass.getSuperclass();
        }
        return theClass;
    }

    public static boolean isCglibProxyClass(Class<?> clazz) {
        return clazz != null && isCglibProxyClassName(clazz.getName());
    }

    public static boolean isCglibProxyClassName(String className) {
        return className != null && className.contains("$$");
    }
}
