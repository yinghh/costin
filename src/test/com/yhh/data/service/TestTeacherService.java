package com.yhh.data.service;

import com.yhh.data.entity.Teacher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class TestTeacherService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private TeacherService teacherService;

    @Test
    public void selectBook() {
        Teacher teacher = teacherService.selectTeacher(1);
        if (teacher != null) {
            logger.info(teacher.toString());
        }
    }


    @Test
    public void deleteBook() {
        Teacher teacher = teacherService.selectTeacher(1);
        if (teacher == null) {
            teacher = new Teacher();
            teacher.setName("数学");
            teacher.setId(1);
            Integer id = teacherService.saveTeacher(teacher);
            logger.info(id.toString());
        }
        teacherService.deleteTeacher(teacher);
    }

    @Test
    public void saveBook() {
        Teacher teacher = teacherService.selectTeacher(2);
        if (teacher != null) {
            teacherService.deleteTeacher(teacher);
        }
        teacher = new Teacher();
        teacher.setName("数学");
        teacher.setId(2);
        Integer id = teacherService.saveTeacher(teacher);
        logger.info(id.toString());
    }

    @Test
    public void updateBook() {
        Teacher teacher = teacherService.selectTeacher(2);
        if (teacher != null) {
            teacherService.deleteTeacher(teacher);
        }
        teacher = new Teacher();
        teacher.setName("数学");
        teacher.setId(2);
        Integer id = teacherService.saveTeacher(teacher);
        logger.info(id.toString());
        teacher.setName("语文");
        teacherService.updateTeacher(teacher);
    }

}
